import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import App from '@/App.vue';

const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(Vuex);

const router = new VueRouter();

describe('App.vue', () => {
  let store;
  let vuetify;

  beforeEach(() => {
    store = new Vuex.Store({
    });
    vuetify = new Vuetify();
  });

  it('has components', () => {
    expect(typeof App.components).toBe('object');
  });

  it('is a Vue instance', () => {
    const wrapper = mount(App, {
      store, 
      router, 
      localVue, 
      vuetify
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});
