var DashboardsMixin = {
    types: {
        summary: "summary",
        impact: "impact",
        proposed_journeys: "proposed_journeys",
        users: "users",
        solidary_users: "solidary_users",
        communities: "communities",
        summary_community: "summary_community",
        hitchhikers: "hitchhikers",
        solidary_carpool: "solidary_carpool",
        community_members: "community_members"

    },
    periodicities: {
        daily: "daily",
        monthly: "monthly"
    },
    watch: {
      communityId(newVal, oldVal){
        this.selected_community = newVal;
      },
      territoryId(newVal, oldVal){
        this.selected_territory = newVal;
        if(newVal!==oldVal){
          this.displayDashboard();
        }
      },
      periodicity(newVal, oldVal){
        this.selected_periodicity = newVal;
        this.displayDashboard();
      }         
    }, 
    computed:{
      dashboards: {
        get: function() {
          return this.dashboard_tabs;
        },
        set: function(dashboard_tabs) {
          this.dashboard_tabs = dashboard_tabs;
        }
      },
      url(){
        return (this.$store.getters['an/item']) ? this.$store.getters['an/item'].url : null;
      },
      date_of_data(){
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        return this.$t("date_data") + " " + yesterday.toLocaleDateString();
      },
      territoryId(){
        if(this.item && this.item.territoryId){
          return this.item.territoryId;
        }
        return null;
      },
      communityId(){
        if(this.item && this.item.communityId){
          return this.item.communityId;
        }
        return null;
      },
      periodicity(){
        return this.$store.getters['an/periodicity']
      },
      fixedPeriodicity(){
        let selectedTab = this.dashboard_tabs.filter( item => item.dashboardId == this.selected_dashboard_tab );
        if(selectedTab[0] && selectedTab[0]['fixedPeriodicity']){
          return selectedTab[0]['fixedPeriodicity'];
        }
        return false;
      }
    },
    methods:{
        displayDashboard() {
          let dashboardId = this.selected_dashboard_tab;
          let territoryId = this.item && this.item.territoryId ? this.item.territoryId : this.selected_territory;
          let communityId = this.selected_community ? this.selected_community : this.item && this.item.communityId ? this.item.communityId : null;
          this.$store.dispatch('an/loadItem', {
            id: dashboardId,
            periodicity: this.selected_periodicity,
            territoryId: territoryId,
            communityId: communityId,
            forceDefaultTerritoryId: this.forceDefaultTerritoryId
          });
        },        
        communityChange(val){
          if (null != val){
            this.dashboard_tabs = this.dashboards_community_filtered
            this.selected_dashboard_tab = DashboardsMixin.types.summary_community
          } else {
            this.dashboard_tabs = this.dashboards_no_community_filter
            this.selected_dashboard_tab = DashboardsMixin.types.summary
          }
          this.item.communityId = val;
          this.selected_community = val;
          this.displayDashboard();
        },
        territoryChange(val){
          this.item.territoryId = val;
          this.selected_territory = val;
          // this.displayDashboard();
        },
        setAndDisplayDashboard(dashboardId) {
          this.selected_dashboard_tab = dashboardId;
          this.displayDashboard();
        },
        filter_with_territory(territory_item) {
          this.selected_territory = territory_item;
        },
        filter_with_community(community_item) {
          this.selected_community = community_item;
        }
    }
};

export default DashboardsMixin;