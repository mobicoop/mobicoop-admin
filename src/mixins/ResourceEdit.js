// Mixin dedicated to resources edit components
import { isEqual } from 'lodash';

var ResourceEditMixin = {
  props: {
    edit: {
      type: Boolean,
      default: false
    }
  },
  data: () => ({
    // validation
    valid: false,
    // first load finished indication : used to check that the item has been loaded and avoid exiting before loaded !
    firstLoadFinished: false,
    // breadcrumb init
    breadcrumb: [],
    // current tab
    mtab: null,
    // dialog
    dialog: false,
    dialogTitle: '',
    dialogText: '',
    dialogAction: '',
    dialogColor: ''
  }),
  mounted() {
    this.createBreadcrumb();
  },
  created() {
    // clear item to avoid unwanted side effects
    this.$store.dispatch(this.resource+'/clearItem');
    if (this.edit) {
      // get item from store for edition
      this.getItem();
    } else {
      // generate new item for create
      this.createItem();
    }
  },
  computed: {
    item() {
      // we use a variable to detect the end of the load, and the end of the first load
      const storeItem = this.$store.getters[this.resource+'/item'];
      // check if first load is finished
      if (this.edit && this.firstLoadFinished === false && storeItem !== null) {
        this.firstLoadFinished = true;
      }
      return storeItem;
    },
    editedItem() {
      const storeEditedItem = this.$store.getters[this.resource+'/editedItem'];
      if (storeEditedItem == null && this.edit && this.firstLoadFinished === true) {
        // we are in edit mode and the item was deleted => we go back to the item list
        this.goParent();
      } else if (storeEditedItem && storeEditedItem.id && !this.edit) {
        // we are in create mode and the item was created => we go back to the item list
        this.goParent();
      }
      return storeEditedItem;
    },
    // local item has been edited
    edited() {
      return (!isEqual(this.item, this.editedItem));
    },
    loading() {
      return this.$store.getters[this.resource+'/loading'];
    },
    // get the name of the parent route
    parentRoute() {
      const route = this.$route;
      return this.$router.options.routes.find( proute => proute.name === route.meta.parent );
    },
    // get the path of the parent route
    parentComponentPath() {
      return this.parentRoute ? this.parentRoute.path : null; 
    },
    // get all the possible fields of the current item 
    // we extract the fields from the informations declared in the tabs
    // it's needed to check which field has been modified, or to handle null values that can be removed in the edit/create process
    fields() {
      const fields = [];
      const tabs = this.tabs;
      tabs.forEach( tab => {
        tab.rows && tab.rows.forEach( row => {
          row.fields.forEach( field => {
            if (field.name !== 'spacer') {
              if (field.value) {
                fields.push({ name: field.name, value: field.value});   
              } else {
                fields.push(field.name); 
              }
            }
          });
        });
        if (tab.component && tab.name) {
          fields.push(tab.name);
        }
      });
      return fields;
    },
    error: {
      get: function() {
        return this.$store.getters[this.resource+'/error'];
      },
      set: function() {
        this.$store.dispatch(this.resource+'/resetError');
      }
    }
  },
  watch: {
    // an error happened
    error(val) {
      if (val) {
        this.$store.dispatch(this.resource+'/resetError');
      }
    }
  },
  methods: {
    // creation of the breadcrumb from the routes
    createBreadcrumb() {
      this.addToBreadcrumb(this.$route);
      this.$store.commit('SET_BREADCRUMB', this.breadcrumb);
    },
    // recursive function to add a route to the breadcrumb
    addToBreadcrumb(route) {
      if (route.meta.parent) {
        const parentRoute = this.$router.options.routes.find( proute => proute.name === route.meta.parent );
        if (parentRoute !== undefined) {
          this.addToBreadcrumb(parentRoute);
        }
      }
      this.breadcrumb.push(
        { 
          text: this.$t('routes.'+route.name+'.breadcrumb'),
          to: !route.skip ? route.path : null,
          exact: true
        }
      );
    },
    // get the item => we ask the store
    getItem() {
      this.$store.dispatch(this.resource+'/loadItem', { id: this.$route.params.id, fields:this.fields});
    },
    // create the item => we ask the store (we send the fields to the store, it will create a blank item)
    createItem() {
      this.$store.dispatch(this.resource+'/createItem', { fields:this.fields});
    },
    // launch dialog action
    launchAction() {
      if (this.dialogAction == 'refresh') {
        this.hardRefresh();
      } else if (this.dialogAction == 'restart') {
        this.hardRestart();
      } else if (this.dialogAction == 'save') {
        this.hardSave();
      } else if (this.dialogAction == 'remove') {
        this.hardRemove();
      }
      this.dialog = false;
    },
    // refresh with confirmation
    refresh() {
      this.dialogAction = 'refresh';
      if (this.edited) {
        this.dialogTitle = this.$t('dialog.refresh.title');
        this.dialogText = this.$t('dialog.refresh.text');
        this.dialogColor = 'primary';
        this.dialog = true;
      } else {
        // nothing was edited, we simply refresh without confirmation
        this.getItem();
      }
    },
    // refresh without confirmation
    hardRefresh() {
      this.getItem();
    },
    // restart with confirmation
    restart() {
      this.dialogAction = 'restart';
      if (this.edited) {
        this.dialogTitle = this.$t('dialog.restart.title');
        this.dialogText = this.$t('dialog.restart.text');
        this.dialogColor = 'primary';
        this.dialog = true;
      } else {
        // nothing was edited, we simply restart without confirmation
        this.$store.dispatch(this.resource+'/initEditedItem');
      }
    },
    // refresh without confirmation
    hardRestart() {
      this.$store.dispatch(this.resource+'/initEditedItem');
      this.$refs.form.resetValidation();
    },
    // save with confirmation
    save() {
      this.dialogAction = 'save';
      this.dialogColor = 'warning';
      if (this.edit) {
        this.dialogTitle = this.$t('dialog.saveEdited.title');
        this.dialogText = this.$t('dialog.saveEdited.text');
      } else {
        this.dialogTitle = this.$t('dialog.save.title');
        this.dialogText = this.$t('dialog.save.text');
      }
      this.dialog = true;
    },
    // save without confirmation
    hardSave() {
      if (this.edit) {
        // save only what have changed
        const id = this.$route.params.id;
        // we check for each store item key/value if it's the same in the local item
        // const differentKeys = Object.keys(this.$store.getters[this.resource+'/item']).filter(k => this.$store.getters[this.resource+'/item'][k] !== this.editedItem[k]);
        const differentKeys = this.getObjectDiff(this.$store.getters[this.resource+'/item'],this.editedItem);
        let item = {};
        // for all values that have changed => we ask the store to... store it 
        differentKeys.forEach((key) => {
          item[key] = this.editedItem[key];
        });
        this.$store.dispatch(this.resource+'/patchItem', { id: id, item: item });
      } else {
        // we are in create mode => save only non null fields
        const nonNullKeys = Object.keys(this.editedItem).filter(k => this.editedItem[k] !== null);
        let item = {};
        nonNullKeys.forEach((key) => {
          item[key] = this.editedItem[key];
        });
        this.$store.dispatch(this.resource+'/postItem', item);
      }
    },
    // confirmation before remove
    remove() {
      this.dialogAction = 'remove';
      this.dialogTitle = this.$t('dialog.remove.title');
      this.dialogText = this.$t('dialog.remove.text');
      this.dialogColor = 'error';
      this.dialog = true;
    },
    // remove without confirmation
    hardRemove() {
      this.$store.dispatch(this.resource+'/deleteItem', this.$route.params.id);
    },
    goParent() {
      this.$router.push(this.parentComponentPath);
    },
    getObjectDiff(obj1, obj2) {
      const diff = Object.keys(obj1).reduce((result, key) => {
        if (!Object.prototype.hasOwnProperty.call(obj2,key)) {
          result.push(key);
        } else if (isEqual(obj1[key], obj2[key])) {
          const resultKeyIndex = result.indexOf(key);
          result.splice(resultKeyIndex, 1);
        }
        return result;
      }, Object.keys(obj2));
      return diff;
    },
  }
};

export default ResourceEditMixin;