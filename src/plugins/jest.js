import Vue from 'vue';
import Vuetify from 'vuetify';
import { config } from '@vue/test-utils';

config.mocks['$t'] = () => {};

Vue.use(Vuetify);
Vue.config.productionTip = false;