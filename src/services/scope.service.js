// service that makes the requests for Scope
import axios from 'axios';
import getEnv from '@/utils/env';

class ScopeService {
  auth() {
    return axios
      .get(getEnv('VUE_APP_SCOPE_URL')+'/login/'+getEnv('VUE_APP_SCOPE_INSTANCE'), { 
        headers: this.getHeaders(),
        withCredentials: true
      })
      .then( () => {
        return Promise.resolve();
      })
      .catch( () => {
        return Promise.reject();
      });
  };
  getHeaders() {
    const token = JSON.parse(localStorage.getItem('token'));
    return { 
      Authorization: 'Bearer ' + token,
    };
  };
}

export default new ScopeService();