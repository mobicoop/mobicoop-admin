import { store } from "../store";

function hasPermission(requiredPermission) {
    const permissions = store.state.auth.permissions;
    const permission = permissions.find(permission => permission === requiredPermission);
    
    return permission !== undefined;
}

export { hasPermission }