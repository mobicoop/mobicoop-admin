import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import Login from '@/views/Login.vue';

const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(Vuex);

const router = new VueRouter();

describe('Login.vue', () => {
  let store;
  let vuetify;

  beforeEach(() => {
    store = new Vuex.Store({
    });
    vuetify = new Vuetify();
  });

  it('is a Vue instance', () => {
    const wrapper = mount(Login, {
      store, 
      router, 
      localVue, 
      vuetify
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});
