_[Documentation fonctionnelle Mobicoop Admin](../README.md) / Communautés / Voir la communauté_
# Voir la communauté


_**Documentation en cours**_ :construction:

[[_TOC_]]



### Visibilité des communautés sécurisées
Un administrateur ne peut voir une communauté sécurisée que s'il en est un des modérateurs ou le créateur.



