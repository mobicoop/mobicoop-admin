_[Documentation fonctionnelle Mobicoop Admin](../README.md) / Inscrits RezoPouce / Ajouter_

# Ajouter un inscrit RezoPouce

_**Documentation en cours**_ :construction:

[[_TOC_]]

### Ajouter un inscrit RezoPouce depuis un inscrit RezoMobicoop déjà existant

1. aller dans Inscrits Rezo Pouce
1. cliquer sur Ajouter
1. saisir l'adresse email de l'utilisateur déjà existant
1. accepter de basculer en édition de l'inscrit Rezo Pouce
1. renseigner tous les champs de son profil RZP (y compris "Identité vérifiée")
1. cliquer sur "Enregistrer"

