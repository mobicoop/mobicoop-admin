_[Documentation fonctionnelle Mobicoop Admin](../README.md) / Structures accompagnantes / Ajouter une structure accompagnante_
# Ajouter une structure accompagnante


_**Documentation en cours**_ :construction:

[[_TOC_]]



### Caractéristiques :construction:

### Objet du déplacement :construction:

### Je suis prêt à...

**Définition**

Les `Je suis prêt à...` sont des services complémentaires qui peuvent être demandés ou attendus de la part de passagers solidaires. Exemples: "monter les courses", "prendre le café", etc.

Chaque `Je suis prêt à...` comprend :

1. une `Valeur (demandeur)`: correspond au nom du `Je suis prêt à...` tel qu'affiché pour le demandeur solidaire (exemple: "monter mes courses")
1. une `Valeur (bénévole)`: correspond au nom du `Je suis prêt à...` tel qu'affiché pour le conducteur bénévole (exemple: "lui monter ses courses")


### Éligibilité bénéficiaire :construction:

### Éligibilité bénévole :construction:

### Opérateurs :construction: